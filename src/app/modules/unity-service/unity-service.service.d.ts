import { OnInit } from '@angular/core';
import { Observable } from "rxjs/Observable";
import 'rxjs/add/observable/fromEvent';
export interface IMessage {
    payload: any;
}
export declare class UnityService implements OnInit {
    private gameInstance;
    messageHandler: String;
    messageMethod: String;
    eventType: String;
    loaderGlobalVariable: String;
    messages: Observable<IMessage>;
    constructor();
    ngOnInit(): void;
    load(componentId: string, buildJson?: string): void;
    registerFlow(observable: Observable<IMessage>): void;
}
